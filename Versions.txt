Version 0.2.6 - 09 / 10 / 2020 :
    - Création du système de boutons
    - Ajout de boutons au menu principal
    - Amélioration du système d'états en jeu
    - Ajout d'un fichier de configuration pour régler la fenêtre du menu principal
    - Nouvelles possibilités du menu principal :
        - Lancer une nouvelle partie
        - Quitter le jeu

Version 0.1.9 - 06 / 10 / 2020 :
    - Ajout d'un nouvel état de type jeu
    - Ajout d'une entité (controlâble pour le moment)
    - Création de raccourcis
    - Ajout de deux fichiers de configuration pour régler les touches utilisables et en jeu
    - Ajout d'un nouvel état de type menu
    - Création de texture pour le menu

Version 0.1.3 - 25 / 09 / 2020 :
    - Ajout du système de temps en jeu
    - Ajout du gestionnaire d'états
    - Ajout d'un fichier de configuration pour régler la fenêtre du jeu