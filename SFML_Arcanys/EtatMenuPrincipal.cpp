#include "stdafx.h"
#include "EtatMenuPrincipal.h"

// Initialisations
void EtatMenuPrincipal::initialiserFonts()
{
	if (!_font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw ("ERREUR::EtatMenuPrincipal::Font 'Dosis-Light.ttf' introuvable");
	}
}

void EtatMenuPrincipal::initialiserRaccourcis()
{
	std::ifstream fichierConfig("Config/touchesMenuPrincipal.ini");

	if (fichierConfig.is_open())
	{
		std::string raccourci = "";
		std::string touche = "";

		while (fichierConfig >> raccourci >> touche)
		{
			_raccourcis[raccourci] = _touchesUtilisable->at(touche);
		}
	}

	fichierConfig.close();
}

void EtatMenuPrincipal::initialiserBoutons()
{
	_boutons["boutonJouer"] = new Bouton(100, 100, 150, 50, &_font, "Nouvelle partie",
		sf::Color(70, 70, 70, 200), sf::Color(200, 200, 200, 255), sf::Color(70, 70, 70, 200));

	_boutons["boutonContinuer"] = new Bouton(100, 200, 150, 50, &_font, "Continuer",
		sf::Color(30, 30, 30, 200), sf::Color(30, 30, 30, 200), sf::Color(30, 30, 30, 200));

	_boutons["boutonEditeur"] = new Bouton(100, 300, 150, 50, &_font, "Mode �diteur",
		sf::Color(70, 70, 70, 200), sf::Color(200, 200, 200, 255), sf::Color(70, 70, 70, 200));

	_boutons["boutonOptions"] = new Bouton(100, 400, 150, 50, &_font, "Options",
		sf::Color(70, 70, 70, 200), sf::Color(200, 200, 200, 255), sf::Color(70, 70, 70, 200));

	_boutons["boutonQuitter"] = new Bouton(100, 500, 150, 50, &_font, "Quitter",
		sf::Color(70, 70, 70, 200), sf::Color(200, 200, 200, 255), sf::Color(70, 70, 70, 200));
}

// Constructeur / Destructeur
EtatMenuPrincipal::EtatMenuPrincipal(sf::RenderWindow* fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats)
	: Etat(fenetre, touchesUtilisable, etats)
{
	initialiserFonts();
	initialiserRaccourcis();
	initialiserBoutons();

	_fondTexture.setSize(sf::Vector2f(_fenetre->getSize().x, _fenetre->getSize().y));
	_fondTexture.setFillColor(sf::Color::Red);
}

EtatMenuPrincipal::~EtatMenuPrincipal()
{
	auto nombre = _boutons.begin();
	for (nombre = _boutons.begin(); nombre != _boutons.end(); nombre++)
	{
		delete nombre->second;
	}
}

// Fonctions
void EtatMenuPrincipal::finEtat()
{
	std::cout << "fin etat menu principal \n";
}

// Mises � jour
void EtatMenuPrincipal::maj(const float& temps)
{
	majPositionSouris();
	majRaccourcis(temps);
	majBoutons();
}

void EtatMenuPrincipal::majRaccourcis(const float& temps)
{
	// Quitter le jeu si touche �chap d�tect�
	verifierQuitter();
}

void EtatMenuPrincipal::majBoutons()
{
	/* Mises � jour de chaque bouton */
	for (auto& bouton : _boutons)
	{
		bouton.second->maj(_sourisPositionVue);
	}

	/* Events clic des boutons */
	// Event : Nouvelle partie
	if (_boutons["boutonJouer"]->getClic())
	{
		_etats->push(new EtatJeu(_fenetre, _touchesUtilisable, _etats));
	}

	// Event : Quitter le jeu
	if (_boutons["boutonQuitter"]->getClic())
	{
		_quitter = true;
	}
}

// Rendu
void EtatMenuPrincipal::rendu(sf::RenderTarget* cible)
{
	if (!cible)
		cible = _fenetre;

	cible->draw(_fondTexture);
	renduBoutons(cible);
}

void EtatMenuPrincipal::renduBoutons(sf::RenderTarget* cible)
{
	for (auto& bouton : _boutons)
	{
		bouton.second->rendu(cible);
	}
}
