#ifndef JEU_H
#define JEU_H

#include "EtatMenuPrincipal.h"

class Jeu
{
private:
	// Variables
	float _temps;

	// Composants
	sf::RenderWindow *_fenetre;
	sf::Event _event;
	sf::Clock _horloge;
	std::stack<Etat*> _etats;
	std::map<std::string, int> _touchesUtilisable;

	// Initialisations
	void initialiserFenetre();
	void initialiserTouches();
	void initialiserEtats();

public:
	// Constructeur / Destructeur
	Jeu();
	virtual ~Jeu();

	// Fonctions
	void finApplication();

	// Mises � jour
	void maj();
	void majSFMLevents();
	void majTemps();

	// Rendu
	void rendu();

	// Coeur
	void lancer();
};

#endif