#include "stdafx.h"
#include "Entite.h"

// Constructeur / Destructeur
Entite::Entite()
{
	_dessin.setSize(sf::Vector2f(64.f, 64.f));
	_vitesseMouvement = 100.f;
	_dessin.setFillColor(sf::Color::White);
}

Entite::~Entite()
{

}

// Fonctions
void Entite::deplacer(const float xDirection, const float yDirection, const float& temps)
{
	_dessin.move(xDirection * _vitesseMouvement * temps, yDirection * _vitesseMouvement * temps);
}

void Entite::maj(const float& temps)
{
	
}

void Entite::rendu(sf::RenderTarget* cible)
{
	cible->draw(_dessin);
}