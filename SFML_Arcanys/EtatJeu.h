#ifndef ETATJEU_H
#define ETATJEU_H

#include "Etat.h"
#include "Entite.h"

class EtatJeu : public Etat
{
private:
	// Variables

	// Composants
	Entite _joueur;

	// Initialisations
	void initialiserRaccourcis();

public:
	// Constructeur / Destructeur
	EtatJeu(sf::RenderWindow* fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats);
	virtual ~EtatJeu();

	// Fonctions
	void finEtat();
	void maj(const float& temps);
	void majRaccourcis(const float& temps);
	void rendu(sf::RenderTarget* cible = NULL);
};

#endif