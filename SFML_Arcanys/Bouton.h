#ifndef BOUTON_H
#define BOUTON_H

enum BoutonEtat
{
	BOUTON_BASE = 0,
	BOUTON_SURVOL,
	BOUTON_CLIC
};

class Bouton
{
private :
	// Variables
	bool _survol, _clic;
	short unsigned _boutonEtat;

	// Composants
	sf::RectangleShape _forme;
	sf::Font* _font;
	sf::Text _texte;

	// Couleurs
	sf::Color _baseCouleur, _survolCouleur, _clicCouleur;

public:
	Bouton(float xPosition, float yPosition, float longueur, float hauteur, 
		sf::Font* font, std::string texte,
		sf::Color baseCouleur, sf::Color survolCouleur, sf::Color clicCouleur);
	~Bouton();

	// Fonctions
	void maj(const sf::Vector2f sourisPosition);
	void rendu(sf::RenderTarget* cible);

	// Accesseurs
	const bool getClic() const;
};

#endif