#ifndef ETATMENUPRINCIPAL_H
#define ETATMENUPRINCIPAL_H

#include "EtatJeu.h"
#include "Bouton.h"

class EtatMenuPrincipal : public Etat
{
private:
	// Variables

	// Composants
	sf::RectangleShape _fondTexture;
	sf::Font _font;
	std::map<std::string, Bouton*> _boutons;

	// Initialisations
	void initialiserFonts();
	void initialiserRaccourcis();
	void initialiserBoutons();

public:
	// Constructeur / Destructeur
	EtatMenuPrincipal(sf::RenderWindow* fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats);
	virtual ~EtatMenuPrincipal();

	// Fonctions
	void finEtat();

	// Mises � jour
	void maj(const float& temps);
	void majRaccourcis(const float& temps);
	void majBoutons();

	// Rendu
	void rendu(sf::RenderTarget* cible = NULL);
	void renduBoutons(sf::RenderTarget* cible = NULL);
};

#endif