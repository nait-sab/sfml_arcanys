#include "stdafx.h"
#include "EtatJeu.h"

// Initialisations
void EtatJeu::initialiserRaccourcis()
{
	std::ifstream fichierConfig("Config/touchesJeu.ini");

	if (fichierConfig.is_open())
	{
		std::string raccourci = "";
		std::string touche = "";

		while (fichierConfig >> raccourci >> touche)
		{
			_raccourcis[raccourci] = _touchesUtilisable->at(touche);
		}
	}

	fichierConfig.close();
}

// Constructeur / Destructeur
EtatJeu::EtatJeu(sf::RenderWindow* fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats)
	: Etat(fenetre, touchesUtilisable, etats)
{
	initialiserRaccourcis();
}

EtatJeu::~EtatJeu()
{

}

// Fonctions
void EtatJeu::finEtat()
{
	std::cout << "fin etat jeu \n";
}

void EtatJeu::maj(const float& temps)
{
	majPositionSouris();
	majRaccourcis(temps);

	_joueur.maj(temps);
}	

void EtatJeu::majRaccourcis(const float& temps)
{
	// Quitter le jeu si touche �chap d�tect�
	verifierQuitter();

	// D�placer le joueur
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(_raccourcis.at("deplacementHaut"))))
		_joueur.deplacer(0.f, -1.f, temps);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(_raccourcis.at("deplacementBas"))))
		_joueur.deplacer(0.f, 1.f, temps);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(_raccourcis.at("deplacementGauche"))))
		_joueur.deplacer(-1.f, 0.f, temps);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(_raccourcis.at("deplacementDroite"))))
		_joueur.deplacer(1.f, 0.f, temps);
}

void EtatJeu::rendu(sf::RenderTarget* cible)
{
	if (!cible)
		cible = _fenetre;
	
	_joueur.rendu(cible);
}