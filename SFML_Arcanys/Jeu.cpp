#include "stdafx.h"
#include "Jeu.h"

// Initialisations
void Jeu::initialiserFenetre()
{
    /* Cr�er une fen�tre SFML en utilisant les options d'un fichier fenetre.ini */
    std::ifstream fichierConfig("Config/fenetre.ini");

    // Config d�faut si fichier introuvable
    std::string titre = "Fichier Config/fenetre.ini introuvable";
    sf::VideoMode fenetreTaille(800, 600);
    unsigned limiteImages = 120;
    bool synchroVerticale = false;

    if (fichierConfig.is_open())
    {
        std::getline(fichierConfig, titre);
        fichierConfig >> fenetreTaille.width >> fenetreTaille.height;
        fichierConfig >> limiteImages;
        fichierConfig >> synchroVerticale;
    }

    fichierConfig.close();

    _fenetre = new sf::RenderWindow(fenetreTaille, titre);
    _fenetre->setFramerateLimit(limiteImages);
    _fenetre->setVerticalSyncEnabled(synchroVerticale);
}

void Jeu::initialiserTouches()
{
    std::ifstream fichierConfig("Config/touchesUtilisable.ini");

    if (fichierConfig.is_open())
    {
        std::string touche = "";
        int valeurID = 0;

        while (fichierConfig >> touche >> valeurID)
        {
            _touchesUtilisable[touche] = valeurID;
        }
    }

    fichierConfig.close();
}

void Jeu::initialiserEtats()
{
    _etats.push(new EtatMenuPrincipal(_fenetre, &_touchesUtilisable, &_etats));
}

// Constructeur / Destructeur
Jeu::Jeu()
{
    initialiserFenetre();
    initialiserTouches();
    initialiserEtats();
}

Jeu::~Jeu()
{
	delete _fenetre;

    while (!_etats.empty())
    {
        delete _etats.top();
        _etats.pop();
    }
}

// Fonctions
void Jeu::finApplication()
{
    std::cout << "Fin de l'application \n";
}

// Mises � jour
void Jeu::maj()
{
    majSFMLevents();

    if (!_etats.empty())
    {
        _etats.top()->maj(_temps);

        if (_etats.top()->getQuitter())
        {
            _etats.top()->finEtat();
            delete _etats.top();
            _etats.pop();
        }
    }
    else // Fin Application
    {
        finApplication();
        _fenetre->close();
    }
}

void Jeu::majSFMLevents()
{
    while (_fenetre->pollEvent(_event))
    {
        if (_event.type == sf::Event::Closed)
            _fenetre->close();
    }
}

void Jeu::majTemps()
{
    /* mettre � jour la variable du temps avec le temps 
    pass� a mettre � jour et faire le rendu d'une image */
    _temps = _horloge.restart().asSeconds();
}

// Rendu
void Jeu::rendu()
{
    _fenetre->clear();

    // rendu des �l�ments
    if (!_etats.empty())
        _etats.top()->rendu(_fenetre);

    _fenetre->display();
}

// Lancer
void Jeu::lancer()
{
    while (_fenetre->isOpen())
    {
        majTemps();
        maj();
        rendu();
    }
}