#ifndef ENTITE_H
#define ENTITE_H

class Entite
{
private:

protected:
	// Variable
	float _vitesseMouvement;

	// Composant
	sf::RectangleShape _dessin;

public:
	// Constructeur / Destructeur
	Entite();
	virtual ~Entite();

	// Fonctions
	virtual void deplacer(const float x, const float y, const float& temps);

	virtual void maj(const float& temps);
	virtual void rendu(sf::RenderTarget* cible);
};

#endif