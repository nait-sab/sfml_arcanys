#ifndef ETAT_H
#define ETAT_H

class Etat
{
protected:
	// Variables
	bool _quitter;

	// Composants
	sf::RenderWindow* _fenetre;
	std::map<std::string, int>* _touchesUtilisable;
	std::map<std::string, int> _raccourcis;
	std::stack<Etat*>* _etats;

	// Souris
	sf::Vector2i _sourisPositionEcran;
	sf::Vector2i _sourisPositionFenetre;
	sf::Vector2f _sourisPositionVue;


	// Initialisations
	virtual void initialiserRaccourcis() = 0;

public:
	// Constructeur / Destructeur
	Etat(sf::RenderWindow *fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats);
	virtual ~Etat();

	// Fonctions
	virtual void finEtat() = 0;
	virtual void verifierQuitter();

	// Mises � jour
	virtual void maj(const float &temps) = 0;
	virtual void majRaccourcis(const float& temps) = 0;
	virtual void majPositionSouris();

	// Rendu
	virtual void rendu(sf::RenderTarget *cible = NULL) = 0;
	
	// Accesseur
	const bool& getQuitter() const;
};

#endif