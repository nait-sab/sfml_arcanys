#include "stdafx.h"
#include "Etat.h"

// Initialisations

// Constructeur / Destructeur
Etat::Etat(sf::RenderWindow* fenetre, std::map<std::string, int>* touchesUtilisable, std::stack<Etat*>* etats)
{
    _fenetre = fenetre;
    _touchesUtilisable = touchesUtilisable;
    _etats = etats;
    _quitter = false;
}

Etat::~Etat()
{
    
}

// Fonctions
void Etat::verifierQuitter()
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(_raccourcis.at("fermer"))))
        _quitter = true;
}

// Mises � jour
void Etat::majPositionSouris()
{
    _sourisPositionEcran = sf::Mouse::getPosition();
    _sourisPositionFenetre = sf::Mouse::getPosition(*_fenetre);
    _sourisPositionVue = _fenetre->mapPixelToCoords(sf::Mouse::getPosition(*_fenetre));
}

// Acceseur
const bool& Etat::getQuitter() const
{
    return _quitter;
}