#include "stdafx.h"
#include "Bouton.h"

// Constructeur / Destructeur
Bouton::Bouton(float xPosition, float yPosition, float longueur, float hauteur,
	sf::Font* font, std::string texte,
	sf::Color baseCouleur, sf::Color survolCouleur, sf::Color clicCouleur)
{
	_boutonEtat = BOUTON_BASE;

	_forme.setPosition(sf::Vector2f(xPosition, yPosition));
	_forme.setSize(sf::Vector2f(longueur, hauteur));

	_font = font;
	_texte.setFont(*font);
	_texte.setString(texte);
	_texte.setFillColor(sf::Color::White);
	_texte.setCharacterSize(12);
	_texte.setPosition(
		_forme.getPosition().x + (_forme.getGlobalBounds().width / 2.f) - _texte.getGlobalBounds().width / 2.f,
		_forme.getPosition().y + (_forme.getGlobalBounds().height / 2.f) - _texte.getGlobalBounds().height
	);

	_baseCouleur = baseCouleur;
	_survolCouleur = survolCouleur;
	_clicCouleur = clicCouleur;
}

Bouton::~Bouton()
{

}

// Fonctions
void Bouton::maj(const sf::Vector2f sourisPosition)
{
	/* Mettre � jour les switchs du survol et clic */
	_boutonEtat = BOUTON_BASE;

	// Survol
	if (_forme.getGlobalBounds().contains(sourisPosition))
	{
		_boutonEtat = BOUTON_SURVOL;

		// Clic
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			_boutonEtat = BOUTON_CLIC;
		}
	}

	switch (_boutonEtat)
	{
		case BOUTON_BASE:
			_forme.setFillColor(_baseCouleur);
			break;

		case BOUTON_SURVOL:
			_forme.setFillColor(_survolCouleur);
			break;

		case BOUTON_CLIC:
			_forme.setFillColor(_clicCouleur);
			break;

		default:
			_forme.setFillColor(sf::Color::Red);
			break;
	}
}

void Bouton::rendu(sf::RenderTarget* cible)
{
	cible->draw(_forme);
	cible->draw(_texte);
}

// Accesseurs
const bool Bouton::getClic() const
{
	if (_boutonEtat == BOUTON_CLIC)
		return true;

	return false;
}